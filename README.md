# miniproj7_hw308
This project aims at achieveing data processing with Vector Database Qdrant.


## Getting started

Tutorials for [semantic search](https://github.com/qdrant/landing_page/blob/master/qdrant-landing/content/documentation/tutorials/search-beginners.md), [Qdrant quick start](https://github.com/qdrant/qdrant/blob/master/QUICK_START.md), and [Rust client for Qdrant vector search engine](https://github.com/qdrant/rust-client)

They include all the content in demand for this project: 

1. All the required installations for Qdrant
2. How to build a semantic search engine from scratch
3. How to run Qdrant with enabled gRPC interface with a complete search example. 

## Instructions

- Create a new cargo project `cargo new miniproj7`
- Add all the dependencies needed to `Cargo.toml`
- Add all the functions `main(), setup_client(), recreate_collection(), ingest_data(), perform_query()`needed in the `main.rs` to fulfill the three requirements: 

1. Ingest data into Vector database
2. Perform queries and aggregations
3. Visualize output

- Run Qdrant first then run cargo in a new tab with commands:

```
# With env variable
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```
```
cargo run 
```

## Screenshots for the requirements and visualilzation

- **main function show all the steps**

![](images/main.png)


- **Ingest data into vector database**

![](images/ingest_data.png)


- **Perform queries and aggregations**

![](images/query_aggregation.png)


- **Visualize output**

![](images/visualize_output.png)


- **Run container**

![](images/run_container.png)



