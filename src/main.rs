use qdrant_client::{
    client::QdrantClient, 
    qdrant::{
        vectors_config::Config, CreateCollection, Distance, PointStruct, SearchPoints, VectorParams, VectorsConfig
    }, serde::PayloadConversionError
};

use anyhow::{Result, anyhow};
use serde_json::json;

// Configuration settings
const QDRANT_URL: &str = "http://localhost:6334";
const COLLECTION_NAME: &str = "my_collection";

#[tokio::main]
async fn main() -> Result<()> {
    let client = setup_client(QDRANT_URL)?;
    recreate_collection(&client, COLLECTION_NAME).await?;
    ingest_data(&client, COLLECTION_NAME).await?;
    perform_query(&client, COLLECTION_NAME).await?;
    Ok(())
}

fn setup_client(url: &str) -> Result<QdrantClient> {
    QdrantClient::from_url(url).build().map_err(|e| anyhow!(e))
}

async fn recreate_collection(client: &QdrantClient, collection_name: &str) -> Result<()> {
    match client.delete_collection(collection_name).await {
        Ok(_) => println!("Existing collection deleted."),
        Err(_) => println!("No existing collection to delete."),
    }

    let collection_creation_result = client.create_collection(&CreateCollection {
        collection_name: collection_name.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await;

    match collection_creation_result {
        Ok(_) => Ok(()),
        Err(e) => Err(anyhow!(e)),
    }
}

async fn ingest_data(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let points = vec![
        PointStruct::new(
            1, 
            vec![0.06, 0.41, 0.66, 0.85], 
            json!({"collge": "Duke University"}).try_into().map_err(|e: PayloadConversionError| anyhow!(e.to_string()))?,
        ),
        PointStruct::new(
            2, 
            vec![0.28, 0.71, 0.55, 0.22], 
        json!({"college": "University of California in Los Angeles"}).try_into().map_err(|e: PayloadConversionError| anyhow!(e.to_string()))?,
        ),
        PointStruct::new(
            3, 
            vec![0.37, 0.62, 0.35, 0.33], 
        json!({"college": "Stanford University"}).try_into().map_err(|e: PayloadConversionError| anyhow!(e.to_string()))?,
        ),
    ];

    client.upsert_points_blocking(collection_name.to_string(), None, points, None).await.map_err(|e| anyhow!(e))?;
    println!("Ingested data sucessfully!"); 

    Ok(())
}

async fn perform_query(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let search_result = client.search_points(&SearchPoints {
        collection_name: collection_name.to_string(),
        vector: vec![0.3, 0.12, 0.8, 0.65],
        limit: 3,
        with_payload: Some(true.into()),
        ..Default::default()
    }).await.map_err(|e| anyhow!(e))?;

    println!("Query Results:"); 
    for result in search_result.result.iter() {
        println!("{:?}\n", result); // Print each result on a new line
    }
    println!("Query completed!"); 
    Ok(())
}



